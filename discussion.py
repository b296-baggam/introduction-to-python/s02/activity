# Pytho control Structures

# Python allows user input with the help of input() method

# username = input("Please enter your name: \n")
# print(f"Hello {username}! Welcome to the Python short course!")
## -------------------------------------------------

# num1 = input("Enter 1st number: ")
# num2 = input("Enter 2nd number: ")
# print(num1 + num2)

# By default, the inpur method outputs are in a form of string, the reason that 2 numbers are joined instead of computed

# The output is the concatenation of the num1 and num2 values. The reason for this is the input() method assigns any value as strings

# ---------------------------------------------------

# To solve this, the value can be typecasted to the necessary data type.

# num1 = int(input("Enter 1st number: "))
# num2 = int(input("Enter 2nd number: \n"))
# print(num1 + num2)

# ---------------------------------

# num1 = input("Enter 1st number: ")
# num2 = input("Enter 2nd number: \n")
# print(int(num1) + int(num2))

# ---------------------------------------------------

# A usual use of user input is for the user to control the application espicially applications with control structures
# Selection control structures - allow the program to choose among choices and run specific codes depending on the choice taken.
# Repitition Control Structures - allow the program to repeat certain blocks of code giving a starting condition and terminating condition

# ------------------------------------------------

# Selection Control Structures
# If-else

# test_num = int(input("Score: "))

# if test_num >= 60:
#     print("Test passed")
# else:
#     print("Test failed")

# -----------------------------------

# test_num2 = int(input("Please enter the 2nd test number: \n"))
# if test_num2 > 0:
#     print("The number is postive.")
# elif test_num2 == 0:
#     print("The number is zero")
# else:
#     print('The number is negative')

# -----------------------------------

# test_num2 = int(input("Please enter the 2nd test number: \n"))
# if test_num2 > 0: print("The number is postive.")
# elif test_num2 == 0: print("The number is zero")
# else: print('The number is negative')

# This works.........

# ---------------------------------------------------

# ------------------- MINI EXERCISE -----------------
    # Create an if-else statement that determine if a number is divisible by 3, 5, or both
    # If the number is divisible by 3, print "The number is divisible by 3"
    ## If the number is divisible by 5, print "The number is divisible by 5"
    # If the number is divisible by 3 and 5, print "The number is divisible by 3 and 5"
    # If the number is not divisible by any, print "The number is not divisible by 3 nor 5"

# -------------- MINI EXERCISE SOLN ---------------

# num3 = int(input("Enter a number: "))
# if num3 % 3 == 0 and num3 % 5 == 0:
#     print("The number is divisible by both 3 and 5")
# elif num3 % 3 == 0:
#     print("The number is divisible by 3")
# elif num3 % 5 == 0:
#     print("The number is divisible by 5")
# else: 
#     print("The number is not divisble by 3 nor by 5")


# if divisible 3
# elif divisible 5
# elif divisible by 3 and 5
# else it not divisible by 3 and 5

# ---------------------------------------------------

# Repetition control structures
# [Section] Loops
# Loops helps a programmer to make an action repeat without repeating the action in code

# Python has loops that can repeat blocks of code

# ----------------------------------

# WHILE loop
# WHILE loops are used to execute a set of statements as long as the condtion is true


# i = 0
# while i <= 5: # 6 is greater than 5
#     print(f"Current count: {i}")
#     i += 1 # Increments the value by 1, and will be re-evaluated by the loop requirement

# ----------------------------------

# FOR loop
# FOR loops are used for iterating over a sequence
# A for loop in Python is used to iterate over a sequence (such as list, tuple, or string)

fruits = ["apple", "mango", "grapes", "cherry", "banana"]

    # list_elements #list
for indiv_fruit in fruits:
    print(indiv_fruit)

# [Section] range() method
# To use the for loop to iterate through values, the range method can be used.

# range() argument:
# range(stop)
for x in range(6): # by default, variable x or any provided variable in this kind of loop starts with zero
    # The codes inside the loop will execute as long as the requirement is satisfied or in "True"
    print(f"The current value is {x}")
    # The range() function defaults to 0 as starting value. As such, this prints the values from 0 to 5.

print("--------------------------------")
# range argument:
# range(start, stop)
for x in range(6, 10):
    print(f"The current value is {x}")

print("--------------------------------")
# range arguments:
# range(start, stop, step)
for x in range(6, 20, 2):
    print(f"The current value is {x}")

print("---------------------------------------")

# ---------------------------------------------------

# [Section] Break statement
# the break statement is used to stop the loop
print("Break------------------------------")
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1
# When j reaches 3 the loop ends and the next iterations will not run

# [Section] Continue Statement
# The continue ends / skips the current iteration and proceeds to the next iteration
print("Continue---------------------------")
k = 0
while k < 6:
    k += 1
    if k == 3:
        continue
    print("hello")
    print(k)

