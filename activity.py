# ------------------ Activity 1 soln ----------------
year = int(input("Please input a year \n"))
if year > 0:
    if year % 400 == 0:
        print(f"{year} is not a leap year.")
    elif year % 4 == 0:
        print(f"{year} is a leap year.")
    else:
        print(f"{year} is not a leap year.")
else:
    print("The given input is invalid.")

# --------------- Activity 2 Soln -------------------
rows = int(input("Enter number of rows \n"))
columns = int(input("Enter number of columns \n"))

for i in range(rows):
    for j in range(columns):
        print("*", end="")
    print("")
